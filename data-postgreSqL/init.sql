/* DROP TABLE IF EXISTS "meta";
DROP TABLE IF EXISTS "data";
DROP TABLE IF EXISTS "failures"; */

CREATE TABLE "meta" (
  "id" SERIAL PRIMARY KEY,
  "last_updated_at" TIMESTAMP WITH TIME ZONE NOT NULL,
  "duration_seconds" DECIMAL NOT NULL
);

CREATE TABLE "data" (
  "id" SERIAL PRIMARY KEY,
  "code" VARCHAR(20) NOT NULL,
  "value" DECIMAL NOT NULL,
  "meta_id" INTEGER REFERENCES "meta"("id") ON DELETE CASCADE
);

CREATE TABLE "failures" (
  "id" SERIAL PRIMARY KEY,
  "timestamp" TIMESTAMP WITH TIME ZONE NOT NULL,
  "duration_seconds" INTERVAL NOT NULL
);