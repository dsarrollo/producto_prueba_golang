package controllers

import (
	"log"
	"net/http"
	"time"

	database "gitlab.com/test-golang2/src/db"
	"gitlab.com/test-golang2/src/services"

	"github.com/go-chi/chi/v5"
)

func HandleCurrency(db *database.PostgresDB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currency := chi.URLParam(r, "currency")

		// Obtener los parámetros de consulta opcionales
		finit := r.URL.Query().Get("finit")
		fend := r.URL.Query().Get("fend")
		if fend != "" {
			date_end, err_end := time.Parse("2006-01-02T15:04:05", fend)
			if err_end != nil {
				services.SendJSONResponse("Formato incorrecto de fend", err_end.Error(), http.StatusBadRequest, w, r)
				return
			}
			date_init := time.Date(2022, time.January, 1, 0, 0, 0, 0, time.UTC)
			// Verificar si se proporcionaron los parámetros de fecha de inicio y fecha final
			if finit != "" {
				// Convertir los parámetros de fecha a objetos time.Time
				date_init, err_init := time.Parse("2006-01-02T15:04:05", finit)
				// Manejar errores de conversión de fecha
				if err_init != nil {
					services.SendJSONResponse("Formato incorrecto de finit", err_init.Error(), http.StatusBadRequest, w, r)
					return
				}
				// ejecutar el servico para consultar la informacion
				data, err := services.GetBetweenDateData(db, currency, date_init, date_end)
				if err != nil {
					services.SendJSONResponse("Error al obtener la data entre fechas finit y fend", err.Error(), http.StatusInternalServerError, w, r)
					return
				}
				services.SendJSONResponse(data, "", http.StatusOK, w, r)
				return
			}
			// ejecutar el servico para consultar la informacion
			data, err := services.GetBetweenDateData(db, currency, date_init, date_end)
			if err != nil {
				services.SendJSONResponse("Error al obtener la data entre fecha fend", err.Error(), http.StatusInternalServerError, w, r)
				return
			}
			services.SendJSONResponse(data, "", http.StatusOK, w, r)
			return
		}
		services.SendJSONResponse("Parametros incorrectos debe existir ambos parametos de fechas de rangos o almenos el parametro fend", "", http.StatusBadRequest, w, r)
		return
	}
}

func HandleAllCurrencies(db *database.PostgresDB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		data, err := services.GetAllData(db)
		if err != nil {
			log.Fatal("Error get all data", err)
			services.SendJSONResponse("", err.Error(), http.StatusInternalServerError, w, r)
			return
		}
		services.SendJSONResponse(data, "", http.StatusOK, w, r)
		return
	}
}
