package controllers

import (
	"os"
	"testing"

	database "gitlab.com/test-golang2/src/db"
)

func TestInitSaveCurrency(t *testing.T) {
	// Configuración de la base de datos
	db := database.GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")

	// Establecer una duración de prueba para INTERVAL_GET_TO_CURRENCYAPI incorrecto
	os.Setenv("INTERVAL_GET_TO_CURRENCYAPI", "uno") // 1 minuto
	// Llamar a la función y capturar el error devuelto
	err := InitSaveCurrency(db)
	// Comprobar que no hay errores
	if err != nil {
		t.Errorf("Se esperaba un error nulo, pero se recibió %v", err)
	}
	os.Unsetenv("API_INTERVAL_GET_TO_CURRENCYAPIKEY")

	// Establecer una duración de prueba para INTERVAL_GET_TO_CURRENCYAPI correcto
	// entra a funcion indefinida y no retorna resultados para testing
}

func TestGetCurrencyOfFreecurrencyapi(t *testing.T) {
	// Configuración de la base de datos
	db := database.GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")

	os.Setenv("TIMEOUT_GET_TO_CURRENCYAPI", "2")
	os.Setenv("API_KEY", "api_key_mal")
	// Ejecutar la función que se está probando
	getCurrencyOfFreecurrencyapi(db)
	os.Unsetenv("TIMEOUT_GET_TO_CURRENCYAPI")
	os.Unsetenv("API_KEY")

	// si TIMEOUT_GET_TO_CURRENCYAPI esta en un formato incorrecto
	os.Setenv("TIMEOUT_GET_TO_CURRENCYAPI", "dos")
	os.Setenv("API_KEY", "api_key_mal")
	// Ejecutar la función que se está probando
	getCurrencyOfFreecurrencyapi(db)
	os.Unsetenv("TIMEOUT_GET_TO_CURRENCYAPI")
	os.Unsetenv("API_KEY")

}
