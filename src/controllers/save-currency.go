package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	database "gitlab.com/test-golang2/src/db"
	"gitlab.com/test-golang2/src/models"
	"gitlab.com/test-golang2/src/services"
)

func InitSaveCurrency(db *database.PostgresDB) error {
	interval, err := time.ParseDuration(os.Getenv("INTERVAL_GET_TO_CURRENCYAPI") + "m")
	if err != nil {
		fmt.Println("Error parsing INTERVAL")
		return err
	}

	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	for range ticker.C {
		go getCurrencyOfFreecurrencyapi(db)
	}
	return nil
}

func getCurrencyOfFreecurrencyapi(db *database.PostgresDB) {
	timeout, err := time.ParseDuration(os.Getenv("TIMEOUT_GET_TO_CURRENCYAPI") + "s")
	if err != nil {
		fmt.Println("Error parsing INTERVAL", err)
		return
	}

	// contexto con tiempo de espera
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	// solicitud HTTP a la API de Free Currency
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://api.currencyapi.com/v3/latest?apikey="+os.Getenv("API_KEY")+"&currencies=&base_currency=ALL", nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	start := time.Now()
	// Realiza la solicitud HTTP y registra un fallo en caso de que se cancele debido al tiempo de espera
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		if ctx.Err() == context.DeadlineExceeded {
			fmt.Println("La llamada a la API superó el tiempo de espera")
			duration := time.Since(start)
			failure := models.Failure{Timestamp: time.Now(), Duration: duration}
			err = services.LogRecords(db, failure)
			if err != nil {
				fmt.Printf("Code 21: %s\n", err)
				return
			}
		}
		fmt.Printf("Code 22: %s\n", err)
		return
	}
	defer resp.Body.Close()
	elapsed := time.Since(start)

	// Leer el cuerpo de la respuesta
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Code 26: %s\n", err)
		return
	}

	var currencyData models.CurrencyData
	err = json.Unmarshal(body, &currencyData)
	if err != nil {
		fmt.Println("Datos incorrectos en json.Unmarshal")
		return
	}

	// Insertar un nuevo registro y obtener su id
	metaID, err := services.SaveMeta(db, currencyData.Meta, elapsed.Seconds())
	if err != nil {
		fmt.Printf("Code 23: %s\n", err)
		return
	}

	//for _, info := range currencyData.Data {
	err = services.SaveData(db, currencyData.Data, metaID)
	if err != nil {
		fmt.Printf("Code 24: %s\n", err)
	}
	//}

	fmt.Println("Datos agregados correctamente")
	return
}
