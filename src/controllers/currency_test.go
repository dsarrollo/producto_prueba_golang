package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	database "gitlab.com/test-golang2/src/db"
	"gitlab.com/test-golang2/src/models"
)

func TestHandleCurrency(t *testing.T) {
	// Configuración de la base de datos
	db := database.GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")

	// Configuración del router
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Get("/currencies/{currency}", HandleCurrency(db))
	/////////////////////////////////////////////////////////////////////////////////////
	// Prueba de la ruta "/currencies/{currency} con las 2 fechas"
	req, _ := http.NewRequest("GET", "/currencies/mxn?finit=2022-10-28T18:15:00&fend=2023-10-28T20:15:00", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("El código de estado no es el esperado. Esperado: %d; Obtenido: %d", http.StatusOK, w.Code)
	}

	// Verificar que la respuesta HTTP tiene un cuerpo no vacío
	var currencyData []models.DataResponse
	err := json.Unmarshal(w.Body.Bytes(), &currencyData)
	if err != nil {
		fmt.Println("Datos incorrectos en json.Unmarshal")
	}
	if len(currencyData) == 0 {
		t.Log("No existen datos en ese rango de fechas o en esa divisa")
	} else {
		t.Errorf("handler returned unexpected body: got %v", w.Body.String())
	}
	/////////////////////////////////////////////////////////////////////////////////////
	// Prueba de la ruta "/currencies/{currency} con la fecha fend correcta"
	req, _ = http.NewRequest("GET", "/currencies/mxn?fend=2023-10-28T20:15:00", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("El código de estado no es el esperado. Esperado: %d; Obtenido: %d", http.StatusOK, w.Code)
	}

	// Verificar que la respuesta HTTP tiene un cuerpo no vacío
	err = json.Unmarshal(w.Body.Bytes(), &currencyData)
	if err != nil {
		fmt.Println("Datos incorrectos en json.Unmarshal")
	}
	if len(currencyData) == 0 {
		t.Log("No existen datos en ese rango de fechas o en esa divisa")
	} else {
		t.Errorf("handler returned unexpected body: got %v", w.Body.String())
	}
	/////////////////////////////////////////////////////////////////////////////////////
	// Prueba de la ruta "/currencies/{currency} con la fecha fend incorrecta"
	req, _ = http.NewRequest("GET", "/currencies/mxn?fend=2023-10-2820:15:00", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("El código de estado no es el esperado. Esperado: %d; Obtenido: %d", http.StatusOK, w.Code)
	}

	// Verificar que la respuesta HTTP tiene un cuerpo no vacío
	err = json.Unmarshal(w.Body.Bytes(), &currencyData)
	if err != nil {
		fmt.Println("Datos incorrectos en json.Unmarshal")
	}
	if len(currencyData) == 0 {
		t.Log("No existen datos en ese rango de fechas o en esa divisa")
	} else {
		t.Errorf("handler returned unexpected body: got %v", w.Body.String())
	}
	/////////////////////////////////////////////////////////////////////////////////////
	// Prueba de la ruta "/currencies/{currency} sin parametros"
	req, _ = http.NewRequest("GET", "/currencies/mxn", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("El código de estado no es el esperado. Esperado: %d; Obtenido: %d", http.StatusOK, w.Code)
	}

	// Verificar que la respuesta HTTP tiene un cuerpo no vacío
	err = json.Unmarshal(w.Body.Bytes(), &currencyData)
	if err != nil {
		fmt.Println("Datos incorrectos en json.Unmarshal")
	}
	if len(currencyData) == 0 {
		t.Log("No existen datos en ese rango de fechas o en esa divisa")
	} else {
		t.Errorf("handler returned unexpected body: got %v", w.Body.String())
	}
	/////////////////////////////////////////////////////////////////////////////////////
	// Prueba de la ruta "/currencies/{currency} con las 2 fechas y fint es incorrecto"
	req, _ = http.NewRequest("GET", "/currencies/mxn?finit=2022-10-2818:15:00&fend=2023-10-28T20:15:00", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("El código de estado no es el esperado. Esperado: %d; Obtenido: %d", http.StatusOK, w.Code)
	}

	// Verificar que la respuesta HTTP tiene un cuerpo no vacío
	err = json.Unmarshal(w.Body.Bytes(), &currencyData)
	if err != nil {
		fmt.Println("Datos incorrectos en json.Unmarshal")
	}
	if len(currencyData) == 0 {
		t.Log("No existen datos en ese rango de fechas o en esa divisa")
	} else {
		t.Errorf("handler returned unexpected body: got %v", w.Body.String())
	}
}

func TestHandleAllCurrencies(t *testing.T) {
	// Configuración de la base de datos
	db := database.GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")

	// Configuración del router
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Get("/currencies/all", HandleAllCurrencies(db))

	req, _ := http.NewRequest("GET", "/currencies/all", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	// Verificar que se recibió una respuesta HTTP con el estado 200
	if w.Code != http.StatusOK {
		t.Errorf("El código de estado no es el esperado. Esperado: %d; Obtenido: %d", http.StatusOK, w.Code)
	}

	// Verificar que la respuesta HTTP tiene un cuerpo no vacío
	var currencyData []models.DataResponse
	err := json.Unmarshal(w.Body.Bytes(), &currencyData)
	if err != nil {
		fmt.Println("Datos incorrectos en json.Unmarshal")
	}
	if len(currencyData) == 0 {
		t.Log("No existen datos")
	} else {
		t.Errorf("handler returned unexpected body: got %v", w.Body.String())
	}
}
