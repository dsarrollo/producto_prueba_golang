package database

import (
	"testing"
	"time"

	_ "github.com/lib/pq"
)

func TestConnectDB(t *testing.T) {
	db := GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")
	if outerr := db.Ping(); outerr != nil {
		t.Error("No se conecto a la base de datos: ", outerr)
	} else {
		t.Log("Conexion exitosa")
	}
}

func TestGetPostgresDB(t *testing.T) {
	// Llamar a la función GetPostgresDB() y verificar que devuelve un objeto no nulo
	db := GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")
	if db == nil {
		t.Error("GetPostgresDB() devolvió un objeto nulo")
	}
}

func TestPostgresDBExec(t *testing.T) {
	// Conectar a la base de datos y ejecutar una consulta SQL
	db := GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")
	now := time.Now()
	_, err := db.Query("INSERT INTO meta (last_updated_at, duration_seconds) VALUES ($1, $2)", now, 0.5)

	// Verificar que no hay errores en la ejecución de la consulta
	if err != nil {
		t.Errorf("Error al ejecutar la consulta: %v", err)
	}
}

func TestPostgresDBQuery(t *testing.T) {
	// Conectar a la base de datos
	db := GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")
	rows, err := db.Query("SELECT * FROM meta WHERE id=$1", 1)

	// Verificar que no hay errores en la ejecución de la consulta
	if err != nil {
		t.Errorf("Error al ejecutar la consulta: %v", err)
	}

	// Verificar que se devuelven las filas esperadas
	var id int
	var last_updated_at time.Time
	var duration_seconds float64
	if rows.Next() {
		err = rows.Scan(&id, &last_updated_at, &duration_seconds)
		if err != nil {
			t.Errorf("Error al escanear las filas: %v", err)
		}
		if id != 1 || duration_seconds != 0.5 {
			t.Errorf("Filas inesperadas: (%d, %s, %b)", id, last_updated_at, duration_seconds)
		}
	} else {
		t.Log("No se encontraron filas")
	}

	// Cerrar las filas y la conexión a la base de datos
	rows.Close()
	db.Close()
}
