package database

import (
	"database/sql"
	"fmt"
	"sync"

	_ "github.com/lib/pq"
)

// Define la estructura para la conexión
type PostgresDB struct {
	DB *sql.DB
}

// Crea una variable global de la conexión
var postgresDB *PostgresDB
var postgresOnce sync.Once

// Función que devuelve una instancia única de la conexión
func GetPostgresDB(connStr string) *PostgresDB {
	// Crear el pool de conexiones a la base de datos
	postgresOnce.Do(func() {
		db, err := sql.Open("postgres", connStr)
		if err != nil {
			fmt.Println("Error al crear la conexion:", err)
		}
		// Establecer la cantidad máxima de conexiones inactivas y activas permitidas en el pool
		db.SetMaxIdleConns(10)
		db.SetMaxOpenConns(100)
		postgresDB = &PostgresDB{DB: db}
	})

	return postgresDB
}

func (p *PostgresDB) QueryRow(query string, args ...interface{}) *sql.Row {
	return p.DB.QueryRow(query, args...)
}

// Método que se puede usar en la estructura de conexión para realizar una consulta
func (p *PostgresDB) Query(query string, args ...interface{}) (*sql.Rows, error) {
	rows, err := p.DB.Query(query, args...)
	if err != nil {
		return nil, err
	}
	return rows, nil
}

func (p *PostgresDB) Close() {
	err := p.DB.Close()
	if err != nil {
		fmt.Println("Error cerrando la conexion:", err)
	}
}

func (p *PostgresDB) Ping() error {
	return p.DB.Ping()
}
