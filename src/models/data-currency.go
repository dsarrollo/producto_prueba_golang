package models

import "time"

type CurrencyData struct {
	Meta MetaStruct            `json:"meta"`
	Data map[string]DataStruct `json:"data"`
}

type MetaStruct struct {
	LastUpdatedAt time.Time `json:"last_updated_at"`
}

type DataStruct struct {
	Code  string  `json:"code"`
	Value float64 `json:"value"`
}

type Failure struct {
	Timestamp time.Time
	Duration  time.Duration
}

type DataResponse struct {
	ID            int
	Code          string
	Value         float64
	LastUpdatedAt time.Time
}
