package services

import (
	database "gitlab.com/test-golang2/src/db"
	"gitlab.com/test-golang2/src/models"
)

func LogRecords(db *database.PostgresDB, failure models.Failure) error {
	_, err := db.Query("INSERT INTO failures(timestamp, duration_seconds) VALUES($1, $2))", failure.Timestamp, failure.Duration)
	return err
}
