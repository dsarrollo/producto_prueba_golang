package services

import (
	"testing"
	"time"

	database "gitlab.com/test-golang2/src/db"
	"gitlab.com/test-golang2/src/models"
)

func TestLogRecords(t *testing.T) {
	// Crea una nueva conexión a la base de datos
	db := database.GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")
	// Configuración de la falla simulada
	failure := models.Failure{
		Timestamp: time.Now(),
		Duration:  5,
	}

	// Configuración para la consulta INSERT correcta
	err := LogRecords(db, failure)
	if err != nil {
		t.Errorf("error recibido: %v", err)
	}
}
