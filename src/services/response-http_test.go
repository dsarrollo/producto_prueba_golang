package services

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSendJSONResponse(t *testing.T) {
	// Crear una respuesta HTTP falsa
	w := httptest.NewRecorder()

	// Crear una solicitud HTTP falsa
	r := httptest.NewRequest(http.MethodGet, "/", nil)

	// Crear datos para la respuesta
	data := map[string]string{"message": "test in this service"}

	// Llamar a la función SendJSONResponse
	SendJSONResponse(data, "test", http.StatusOK, w, r)

	// Verificar que el encabezado y el cuerpo de la respuesta son correctos
	if w.Header().Get("Content-Type") != "application/json; charset=utf-8" {
		t.Errorf("Content-Type esperado: application/json; charset=utf-8, obtenido: %s", w.Header().Get("Content-Type"))
	}

	if w.Code != http.StatusOK {
		t.Errorf("Código de estado HTTP esperado: %d, obtenido: %d", http.StatusOK, w.Code)
	}

	var response MessageResponseHTTP
	if err := json.NewDecoder(w.Body).Decode(&response); err != nil {
		t.Errorf("Error decodificando respuesta JSON: %s", err)
	}

	if response.Data == nil {
		t.Errorf("Data no debería ser nulo")
	}

	// Verificar que los datos de la respuesta son correctos
	if data["message"] != response.Data.(map[string]interface{})["message"] {
		t.Errorf("Mensaje de respuesta incorrecto")
	}
}
