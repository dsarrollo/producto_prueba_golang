package services

import (
	"encoding/json"
	"net/http"
)

type MessageResponseHTTP struct {
	Data      interface{} `json:"data" bson:"data"`
	ErrorData string      `json:"error_data" bson:"error_data"`
}

// SendJSONResponse return a path of binary
func SendJSONResponse(data interface{}, errorData string, status int, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	response := MessageResponseHTTP{Data: data, ErrorData: errorData}
	json.NewEncoder(w).Encode(response)
}
