package services

import (
	"strings"
	"time"

	database "gitlab.com/test-golang2/src/db"
	"gitlab.com/test-golang2/src/models"
)

func SaveMeta(db *database.PostgresDB, currencyMeta models.MetaStruct, duration float64) (int, error) {
	// Define la consulta SQL
	const sql = "INSERT INTO meta(last_updated_at, duration_seconds) VALUES($1, $2) RETURNING id"
	// Ejecuta la consulta y captura el resultado
	var metaId int
	err := db.QueryRow(sql, currencyMeta.LastUpdatedAt, duration).Scan(&metaId)
	if err != nil {
		return 0, err
	}
	// Retorna el id del registro insertado
	return metaId, nil
}

func SaveData(db *database.PostgresDB, data map[string]models.DataStruct, metaID int) error {
	stmt, err := db.DB.Prepare("INSERT INTO data(code, value, meta_id) VALUES($1, $2, $3)")
	if err != nil {
		return err
	}
	defer stmt.Close()

	for _, value := range data {
		_, err := stmt.Exec(value.Code, value.Value, metaID)
		if err != nil {
			return err
		}
	}
	return nil
}

func GetBetweenDateData(db *database.PostgresDB, currency string, date_init time.Time, date_end time.Time) ([]models.DataResponse, error) {
	var dataList []models.DataResponse
	rows, err := db.Query("SELECT data.id, data.code, data.value, meta.last_updated_at FROM data INNER JOIN meta ON data.meta_id=meta.id WHERE meta.last_updated_at BETWEEN $1 AND $2 AND data.code=$3", date_init, date_end, strings.ToUpper(currency))
	if err != nil {
		return dataList, err
	}
	defer rows.Close()
	// Recorre los resultados de la consulta
	for rows.Next() {
		var data models.DataResponse
		err := rows.Scan(&data.ID, &data.Code, &data.Value, &data.LastUpdatedAt)
		if err != nil {
			return dataList, err
		}
		dataList = append(dataList, data)
	}
	return dataList, err
}

func GetAllData(db *database.PostgresDB) ([]models.DataResponse, error) {
	var dataList []models.DataResponse
	rows, err := db.Query("SELECT data.id, data.code, data.value, meta.last_updated_at FROM data INNER JOIN meta ON data.meta_id = meta.id;")
	defer rows.Close()
	if err != nil {
		return dataList, err
	}
	// Recorre los resultados de la consulta

	for rows.Next() {
		var data models.DataResponse
		err := rows.Scan(&data.ID, &data.Code, &data.Value, &data.LastUpdatedAt)
		if err != nil {
			return dataList, err
		}
		dataList = append(dataList, data)
	}
	return dataList, err
}
