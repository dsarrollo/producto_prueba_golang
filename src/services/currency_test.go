package services

import (
	"testing"
	"time"

	database "gitlab.com/test-golang2/src/db"
	"gitlab.com/test-golang2/src/models"
)

func TestSaveAndGetData(t *testing.T) {
	// Crea una nueva conexión a la base de datos
	db := database.GetPostgresDB("postgres://postgres:postgres@127.0.0.1:5433/currency_test?sslmode=disable")

	// Crea una nueva estructura MetaStruct
	meta := models.MetaStruct{LastUpdatedAt: time.Now()}

	// Llama a la función SaveMeta para guardar la estructura MetaStruct en la base de datos
	metaId, err := SaveMeta(db, meta, 10.5)
	if err != nil {
		t.Errorf("Error saving meta data: %v", err)
	}

	// Guarda algunos datos de prueba en la base de datos
	// Definir un slice de DataStruct
	data := []models.DataStruct{
		{Code: "USD", Value: 1.0},
		{Code: "EUR", Value: 0.83},
		{Code: "GBP", Value: 0.72},
	}

	// Crear un map vacío
	currencyData := make(map[string]models.DataStruct)

	// Llenar el map con los datos del slice
	for _, d := range data {
		currencyData[d.Code] = d
	}
	err = SaveData(db, currencyData, metaId)
	if err != nil {
		t.Errorf("Error saving data: %v", err)
	}

	// Obtén todos los datos de la base de datos
	dataList, err := GetAllData(db)
	if err != nil {
		t.Errorf("Error getting all data: %v", err)
	}

	// Comprueba que se hayan guardado todos los datos
	if len(dataList) != 2 {
		t.Errorf("Expected 2 data records, got %d", len(dataList))
	}

	// Obtén los datos entre dos fechas
	startDate := time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
	endDate := time.Now()

	dataList, err = GetBetweenDateData(db, "USD", startDate, endDate)
	if err != nil {
		t.Errorf("Error getting data between dates: %v", err)
	}

	// Comprueba que se hayan obtenido los datos correctamente
	if len(dataList) != 1 {
		t.Errorf("Expected 1 data record, got %d", len(dataList))
	}

	// Limpia la base de datos
	_, err = db.Query("DELETE FROM data;")
	if err != nil {
		t.Errorf("Error cleaning data: %v", err)
	}

	_, err = db.Query("DELETE FROM meta;")
	if err != nil {
		t.Errorf("Error cleaning meta data: %v", err)
	}
}
