package routes

import (
	"net/http"

	"gitlab.com/test-golang2/src/controllers"
	database "gitlab.com/test-golang2/src/db"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func Router(db *database.PostgresDB) http.Handler {
	r := chi.NewRouter()
	// Middleware
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Get("/currencies/{currency}", controllers.HandleCurrency(db))
	r.Get("/currencies/all", controllers.HandleAllCurrencies(db))
	return r
}
