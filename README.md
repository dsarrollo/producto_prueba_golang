**Producto de prueba en Go**
Este es un proyecto de prueba que implementa un servidor web en Go. Proporciona un conjunto básico de endpoints para demostrar cómo crear una API RESTful en Go. Este proyecto también incluye algunas pruebas unitarias básicas y se puede usar como punto de partida para proyectos más grandes.

**Dependencias**
Este proyecto utiliza las siguientes dependencias:

chi - Un enrutador HTTP para Go.
testify - Un conjunto de herramientas de prueba para Go.

**Instalación**
Para instalar este proyecto, primero debes clonar el repositorio:

sh
Copy code
git clone https://gitlab.com/desarrollo/producto_prueba_golang.git
Luego, asegúrate de tener las dependencias instaladas:

sh
Copy code
go mod tidy
Finalmente, puedes compilar y ejecutar el proyecto:

sh
Copy code
go build .
./test-golang2

Para mayor comodidad puedes usar docker, en la carpeta "data-postgreSqL" esta el docker compose de la base de datos de pruebas (currency_test) y la de pdesarrollo (currency)
solo basta con ejecutar "docker compose up" desde la terminar en la ruta de "data-postgreSqL" y ya estara las base de datos POSTGRESQL activas.

Tambien hay un archivo docker compose para ejecutar el proyecto go solo debe ejecutar el go build si no existe el binario y luego ejecutar "docker compose up" desde la terminar en la ruta raiz y estaria el servidor activo

Uso
Una vez que el servidor está en ejecución, puedes acceder a él desde tu navegador o utilizar una herramienta como Postman para hacer solicitudes a los diferentes endpoints. Aquí hay una lista de los endpoints disponibles:

1. GET http://localhost:8080/currencies/mxn?finit=2021-10-28T18:15:00&fend=2023-04-13T23:59:59 - Obtiene una lista de divisas en un rango de fechas
2. GET http://localhost:8080/currencies/all - Obtiene todas las divisas registradas


**Pruebas**
Este proyecto incluye algunas pruebas unitarias básicas que puedes ejecutar para asegurarte de que todo funcione como se espera. Para ejecutar las pruebas, simplemente ejecuta el siguiente comando:

sh
Copy code
go test -coverprofile=coverage.out ./...

Tambien existe un archivo coverage.html para mostrar las pruebas ya ejecutadas


**IMPORTANTE**: el archivo de configuración es .env, si se usara docker la configuración estará en environment


**Contribuir**
Si quieres contribuir a este proyecto, por favor envía un pull request desde una rama separada. Asegúrate de incluir pruebas para cualquier cambio que hagas y de seguir las guías de estilo de código de Go.

**Licencia**
Este proyecto está bajo la Licencia MIT. Consulta el archivo LICENSE para más detalles.
