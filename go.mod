module gitlab.com/test-golang2

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.7
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
