package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/test-golang2/src/controllers"
	database "gitlab.com/test-golang2/src/db"
	"gitlab.com/test-golang2/src/routes"

	"github.com/joho/godotenv"
)

func main() {
	// Crear una conexión en postgre
	// Obtiene una instancia única de la conexión
	db := database.GetPostgresDB(os.Getenv("POSTGRE_URL"))
	defer db.Close()
	// verificar que el archivo .env este presente
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env (ignore if run docker compose)")
	}
	// iniciar el proceso de recoleccion de informacion de las divisas
	go controllers.InitSaveCurrency(db)

	// iniciar el api rest
	addr := os.Getenv("API_URL")
	log.Println("Serving at " + addr)
	http.ListenAndServe(addr, routes.Router(db))
}
